<?php

require_once 'config.php';

if (!isset($_REQUEST['cat'])) {
  redirect('/');
}

$cat = $_REQUEST['cat'];

$nav = array(
		'stab' => array(
				'xpath' => "//product[category='������������ ����������']",
				'caption' => "������������� ����������",
				'title' => "������������� ���������� | �������� ������� �������������������",
				'keywords' => "����������� ����������",
				'description' => ""
				),
		'stab1' => array(
				'xpath' => "//product[category='������������ ����������' and phases=1]",
				'caption' => "���������� ������������� ����������",
				'title' => "���������� ������������� ���������� | �������� ������� �������������������",
				'keywords' => "���������� ����������� ����������",
				'description' => ""
				),
		'stab3' => array(
				'xpath' => "//product[category='������������ ����������' and phases=3]",
				'caption' => "���������� ������������� ����������",
				'title' => "���������� ������������� ���������� | �������� ������� �������������������",
				'keywords' => "���������� ������������ ����������",
				'description' => ""
				),
		'generators' => array(
				'xpath' => "//product[category='��������������' or category='�������������']",
				'caption' => "�����������������",
				'title' => "�����������������, �������������� | �������� ������� �������������������",
				'keywords' => "��������� ���������������� �������������� �������������",
				'description' => ""
				),
		'benzogen' => array(
				'xpath' => "//product[category='��������������']",
				'caption' => "���������������",
				'title' => "��������������� (���������� �����������������) | �������� ������� �������������������",
				'keywords' => "",
				'description' => ""
				),
		'gazogen' => array(
				'xpath' => "//product[category='�������������']",
				'caption' => "��������������",
				'title' => "�������������� (������� �����������������) | �������� ������� �������������������",
				'keywords' => "",
				'description' => ""
				),
		'converter' => array(
				'xpath' => "//product[category='��������']",
				'caption' => "��������������� ����������",
				'title' => "��������������� ���������� | �������� ������� �������������������",
				'keywords' => "",
				'description' => ""
				),
		'invertor' => array(
				'xpath' => "//product[category='��������' and case=0]",
				'caption' => "��������� (��������������� ����������)",
				'title' => "��������� (��������������� ����������) | �������� ������� �������������������",
				'keywords' => "",
				'description' => ""
				)
            );

if (file_exists(dirname(__FILE__) . '/custom/custom_cat.php')) {
  include dirname(__FILE__) . '/custom/custom_cat.php';
}

if (!isset($nav[$cat])) {
  redirect('/');
}

$products = getProductsArray($nav[$cat]['xpath']);

$tmpl = startTemplate('cat.tmpl');
$tmpl->setLoop('products', $products);
foreach ($nav[$cat] as $key => $value)
{
  $tmpl->setVar($key, $value);
}
$tmpl->pparse();

?>