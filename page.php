<?php

require_once 'config.php';

if ((!isset($_GET['page'])) || (!preg_match('/^([0-9]|[a-zA-Z_])*$/', $_GET['page'])) ||
   ((!file_exists( dirname(__FILE__) . '/custom/pages/' . $_GET['page'] . '.htm' ) && (!file_exists( dirname(__FILE__) . '/pages/' . $_GET['page'] . '.htm' ))))) {

  redirect('/');

}

if (file_exists(dirname(__FILE__) . '/custom/pages/' . $_GET['page'] . '.htm'))
{
  $text = file_get_contents(dirname(__FILE__) . '/custom/pages/' . $_GET['page'] . '.htm');
} else {
  $text = file_get_contents(dirname(__FILE__) . '/pages/' . $_GET['page'] . '.htm');
}

$text = processExtendedTags($text);

while (preg_match("/<TMPL_INCLUDE\s*FILE=\"(.*?)\">/i", $text, $matches))
{
  if (file_exists(dirname(__FILE__) . $matches[1]))
  {
    $incl_content = file_get_contents( dirname(__FILE__) . $matches[1] );
  } else {
    $incl_content = '';
  }
  $text = preg_replace("/<TMPL_INCLUDE(.*?)>/i", $incl_content, $text, 1);
}

preg_match_all("/<TMPL_VAR\s*NAME=\"(.*?)\"\s*VALUE=\"(.*?)\">/i", $text, $matches, PREG_SET_ORDER);
preg_match_all("/<TMPL_EXPORT\s*NAME=[\"'](.*?)[\"']\s*VALUE=[\"'](.*?)[\"']>/i", $text, $matches2, PREG_SET_ORDER);

$text = preg_replace("/<TMPL_VAR(.*?)>\r*\n*/", "", $text);
$text = preg_replace("/<TMPL_EXPORT(.*?)>\r*\n*/", "", $text);

$tmpl = startTemplate('page.tmpl');
$tmpl->setvar('html', $text);
for ($i=0; $i< count($matches); $i++)
  $tmpl->setvar($matches[$i][1], $matches[$i][2]);
for ($i=0; $i< count($matches2); $i++)
  $tmpl->setvar($matches2[$i][1], $matches2[$i][2]);
$tmpl->pparse();

?>