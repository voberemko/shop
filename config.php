<?php

$SITE_ID = 2771;
$AFF_ID = 1;

$TMPL_DIR = dirname(__FILE__) . '/theme/';
$VERSION_INFO_FILE 	= dirname(__FILE__) . '/version.txt';
$BILLING_SERVER = 'billing.powerpartners.ru';
$UPGRADE_INTERVAL = 60*60*24;

date_default_timezone_set('Europe/Moscow');

require_once 'util.php';
require_once 'vlib/vlibTemplate.php';

?>