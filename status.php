<?php

require_once 'config.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$order = urlencode($_POST['order_no']);
	$req = "order=$order";

	$email = urlencode($_POST['email']);
	$req .= "&email=$email";

	$status = ServerRequest($req, $BILLING_SERVER, '/status.php');


	if (preg_match('/NO_ORDER/', $status, $matches)) {
        	$tmpl = startTemplate('status.tmpl');
		$tmpl->setVar('error', 1);
		$tmpl->setVar('email', urldecode($email));
		$tmpl->setVar('order', $order);
 	} else {
       		$tmpl = startTemplate('status_ok.tmpl');
		$tmpl->setVar('status', $status);
		foreach ( split('&', $status) as $item ) {
			list($key, $value) = split("=", $item, 2);
				$tmpl->setVar($key, urldecode($value));
        	}
        }
       	$tmpl->pparse();

	exit;
}

$tmpl = startTemplate('status.tmpl');
$tmpl->pparse();

?>