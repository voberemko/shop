$(document).ready(function(){
	$(':submit[name=cart_submit]').hide();
	$('input.cart_product_qty').numeric(false, null);
	$('select[name=delivery]').bind('change', onDeliveryChange);
	$('input.cart_product_qty').bind('keyup', onKeyup);
	$('input.cart_product_qty').bind('paste', function() { setTimeout(updateCart, 100) } );
	$('a[href^=remove.php]').bind('click', removeProduct);
	
	$('input[name=customer]').bind('click', onCustomerClick);
	$('input[name=payment]').bind('click', onPaymentClick);
	$('input[name=customer][value=1]').click();
	
	$('form[name=order]').validate({
		showErrors: function() {},
		ignore: '.hidden',
		invalidHandler: function(form, validator) {
			if (validator.numberOfInvalids() > 0) {
				alert(validator.errorList[0].message);
				$(validator.invalidElements()[0]).focus();
			}
		},
		rules: {
			lname: {
				required: true,
				minlength: 2
			},
			fname: {
				required: true,
				minlength: 2
			},
			mname: {
				required: true,
				minlength: 2
			},
			fiz_lname: {
				required: true,
				minlength: 2
			},
			fiz_fname: {
				required: true,
				minlength: 2
			},
			fiz_mname: {
				required: true,
				minlength: 2
			},
			fiz_address: {
				required: true,
				minlength: 2
			},
			email: {
				required: true,
				email: true
			},
			wmid: {
				required: true,
				number: true
			},
			yur_payer: {
				required: true,
				minlength: 2
			},
			yur_address: {
				required: true,
				minlength: 2
			},
			yur_inn: {
				required: true,
				number: true
			},
			yur_kpp: {
				required: true,
				number: true
			},
			yur_rs: {
				required: true,
				number: true
			},
			yur_bank: {
				required: true,
				minlength: 2
			},
			yur_ks: {
				required: true,
				number: true
			},
			yur_bik: {
				required: true,
				number: true
			},
			phone: {
				required: true,
				minlength: 2
			},
			address: {
				required: true,
				minlength: 2
			}
		},
		messages: {
			lname: {
				required: '���������� ������� ������� ����������.',
				minlength: '������� �� ������ ���� ������ 2-� ��������'
			},
			fname: {
				required: '���������� ������� ��� ����������.',
				minlength: '��� �� ������ ���� ������ 2-� ��������'
			},
			mname: {
				required:'���������� ������� �������� ����������.',
				minlength: '�������� �� ������ ���� ������ 2-� ��������'
			},
			fiz_lname: {
				required: '���������� ������� ������� �����������.',
				minlength: '������� �� ������ ���� ������ 2-� ��������'
			},
			fiz_fname: {
				required: '���������� ������� ��� �����������.',
				minlength: '��� �� ������ ���� ������ 2-� ��������'
			},
			fiz_mname: {
				required: '���������� ������� �������� �����������.',
				minlength: '�������� �� ������ ���� ������ 2-� ��������'
			},
			fiz_address: {
				required: '���������� ������� ����� �������� �����������.',
				minlength: '����� �� ������ ���� ������ 2-� ��������'
			},
			email: {
				required: '���������� ������� ���������� email-�����.  �� ���� ����� ������ ����.',
				email: '����� ����������� ����� ����� �� ���������.'
			},
			wmid: {
				required: '���������� ������� WMID ������ WebMoney ��������.',
				number: 'WMID WebMoney �������� ����� �� ���������.'
			},
			yur_payer: {
				required: '���������� ������� ������������ ����������� ���������.',
				minlength: '�������� �� ������ ���� ������ 2-� ��������'
			},
			yur_address: {
				required: '���������� ������� ����������� ����� ����������� ���������.',
				minlength: '����� �� ������ ���� ������ 2-� ��������'
			},
			yur_inn: {
				required: '���������� ������� ���.',
				number: '��� ����� �� ���������.'
			},
			yur_kpp: {
				required: '���������� ������� ���.',
				number: '��� ����� �� ���������.'
			},
			yur_rs: {
				required: '���������� ������� ��������� ����.',
				number: '��������� ���� ����� �� ���������.'
			},
			yur_bank: {
				required: '���������� ������� ������������ ��������� ����� ���������.',
				minlength: '����������� ����� �� ������ ���� ������ 2-� ��������'
			},
			yur_ks: {
				required: '���������� ������� ����������������� ����.',
				number: '����������������� ���� ����� �� ���������.'
			},
			yur_bik: {
				required: '���������� ��� �����.',
				number: '��� ����� ����� �� ���������.'
			},
			phone: {
				required: '���������� ������� ���������� �������.',
				minlength: '������� �� ������ ���� ������ 2-� ��������'
			},
			address: {
				required: '���������� ������� ����� ��������.',
				minlength: '����� �� ������ ���� ������ 2-� ��������'
			}
		}
	});
});

/////////////////////////////////////////////////////////////////////////////////////////////
(function($) {
	$.fn.myEnable = function(value) {
		return this.each(function(){
			if ( $(this).is('input:radio') ) {
				if ( $(this).next().is('label') ) {
					if ((value) & ($(this).next().data('LastColor') != 0)) {
						$(this).next().css("color", $(this).next().data('LastColor'));
					} else {
						$(this).next().data('LastColor',  $(this).next().css("color"));
						$(this).next().css("color", "gray");
					}
				}
				return $(this).attr("disabled", value ? '' : true);
			}
		});
	}
})(jQuery)

/////////////////////////////////////////////////////////////////////////////////////////////
function onKeyup() 
{
	if ($(this).data('old_value') != $(this).val()) 
	{
		updateCart();
		$(this).data('old_value', $(this).val());
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////
function updateCart()
{
	var total_cost = 0;
	var total_qty = 0;
	var total_weight = 0;
	var total_products = 0;
	$('.cart_product').each( function(){
		var qty = $(this).find('input.cart_product_qty').val();
		qty = qty.replace(/[\D]/g, '');
		$(this).find('input.cart_product_qty').val(qty);
		var price = $(this).find('.cart_product_price').text();
		var weight = $(this).find('.cart_product_weight').attr('weight');
		$(this).find('.cart_product_cost').text( price*qty );
		$(this).find('.cart_product_weight').text( (weight*qty).toFixed(1) )
		total_cost += price*qty;
		total_qty += (qty.length > 0) ? parseInt(qty) : 0;
		total_weight +=  parseFloat(weight*qty);
		total_products++;
	} );
	if (total_products == 0) {
		window.location='cart.php';
		exit;
	}
	delivery_free_limit = $('select[name=delivery] option:selected').attr('free_limit');
	delivery_cost = (delivery_free_limit > total_cost) ? $('select[name=delivery] option:selected').attr('price') : 0;
	total_cost += parseInt(delivery_cost);
	$('.delivery_cost').text(delivery_cost);
	$('.cart_total_cost').text(total_cost);
	$('.cart_total_qty').text(total_qty);
	$('.cart_total_weight').text(total_weight.toFixed(1));
	
	$.ajax({		
		type: 'POST',
		url: $('form[name=cart]').attr('action'),
		dataType: 'html',
		data: $('form[name=cart]').serializeArray()
	});
}

/////////////////////////////////////////////////////////////////////////////////////////////
function removeProduct(eventObj)
{
	var product_name = $(this).parentsUntil('.cart_product').siblings('.cart_product_name').text();
	if ( confirm("������� �� ������ ������ \n" + product_name + "?") )
	{
		if ($('.cart_product').size() > 1)
		{
			eventObj.preventDefault();
			$.ajax({		
				type: 'GET',
				url: $(this).attr("href")
			});
			$(this).closest('.cart_product').remove();
			updateCart();
		}
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////
function onCustomerClick()
{
	if ($(this).val() == 1) {
		$('input[name=payment][value=1]').myEnable(true);
		$('input[name=payment][value=4]').myEnable(true);
		$('input[name=payment]:checked').click();
	}
	if ($(this).val() == 2) {
		$('input[name=payment][value=1]').myEnable(false);
		$('input[name=payment][value=4]').myEnable(false);
		$('input[name=payment][value=2]').click();
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////
function onPaymentClick()
{
	$('.person').show().find('input').removeClass('hidden');
	if ($(this).val() == 1) {
		$('.noncash').hide().find('input').addClass('hidden');
	}
	if ($(this).val() == 2) {
		$('.noncash').hide().find('input').addClass('hidden');
		if ($('input[name=customer]:checked').val() == 2) {
			$('.noncash_title, .yur').show().find('input').removeClass('hidden');
		} else {
			$('.noncash_title, .fiz').show().find('input').removeClass('hidden');
			$('.person').hide().find('input').addClass('hidden');
		}
	}
	if ($(this).val() == 4) {
		$('.yur, .fiz').hide().find('input').addClass('hidden');
		$('.noncash_title, .wm').show().find('input').removeClass('hidden');
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////
function onDeliveryChange()
{
	if ($(this).val() == 1) {
		$('.delivery_address').hide().find('input').addClass('hidden');
	} else {
		$('.delivery_address').show().find('input').removeClass('hidden');
	}
	updateCart();
}